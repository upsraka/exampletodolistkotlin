package com.example.exampletodolistkotlin.page

import android.content.Intent
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exampletodolistkotlin.adapter.DefaultLoadStateAdapter
import com.example.exampletodolistkotlin.adapter.DividerItemDecoration
import com.example.exampletodolistkotlin.R
import com.example.exampletodolistkotlin.adapter.NoteRecyclerViewAdapter
import com.example.exampletodolistkotlin.adapter.RecyclerViewItemClickListener
import com.example.exampletodolistkotlin.baseActivity.BaseActivity
import com.example.exampletodolistkotlin.databinding.ActivityMainBinding
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import io.objectbox.reactive.DataSubscription
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Created by Raka Putra on 1/4/21
 * Jakarta, Indonesia.
 */
class MainActivity constructor(): BaseActivity() {
    private val binding: ActivityMainBinding by binding(R.layout.activity_main)
    val viewModel: MainActivityViewModel by viewModels()

    private lateinit var noteAdapter: NoteRecyclerViewAdapter
    private lateinit var subscription: DataSubscription

    companion object {
        val INTENT_EXTRA_KEY = "NOTE_ID"
    }

    override fun setupView() {
        super.setupView()
        binding.apply {
            lifecycleOwner = this@MainActivity
            mainActivity = viewModel
        }
    }

    override fun setupListener() {
        super.setupListener()
        fab_save.setOnClickListener {
            startBorrowItemActivity()
        }
    }

    private fun startBorrowItemActivity(id: Long? = null) {
        val intent = Intent(this, NoteAddActivity::class.java)
        if (id != null) {
            intent.putExtra(INTENT_EXTRA_KEY, id)
        }
        startActivity(intent)
    }

    override fun setupObserver() {
        super.setupObserver()
        noteAdapter = NoteRecyclerViewAdapter(object : RecyclerViewItemClickListener {
            override fun onItemClicked(note: NoteEntity) {
                startBorrowItemActivity(note.id)
            }
        })
        recyclerView.adapter = noteAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this))
        recyclerView.apply {
            adapter = noteAdapter.withLoadStateFooter(DefaultLoadStateAdapter())
        }

        lifecycleScope.launch {
            viewModel.noteList.collect() {
                noteAdapter.submitData(it)

            }
        }
    }

    override fun onDestroy() {
        subscription.cancel()
        super.onDestroy()
    }
}
