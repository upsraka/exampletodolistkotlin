package com.example.exampletodolistkotlin.adapter

import androidx.paging.PagingSource
import com.example.exampletodolistkotlin.persistance.dao.NoteDao
import com.example.exampletodolistkotlin.persistance.dao.NoteDaoImp
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import com.example.exampletodolistkotlin.repository.NoteRepository
import kotlinx.coroutines.delay
import java.io.IOException

/**
 * Created by Raka Putra on 1/7/21
 * Jakarta, Indonesia.
 */
private const val PAGE_INDEX = 1


class NotePagingSource(
        private var noteDao: NoteDao,
        private var repository: NoteRepository
) : PagingSource<Int, NoteEntity>() {

    init {
        noteDao = NoteDaoImp()
        repository = NoteRepository(noteDao)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NoteEntity> {
        val position = params.key ?: PAGE_INDEX
        return try {
            val listNotes = mutableListOf<NoteEntity>()
            val list = repository.pagingNote(position.toLong(), 3)
            list?.let { listNotes.addAll(it) }
            delay(3000)
            LoadResult.Page(
                    data = listNotes,
                    prevKey = if (position == PAGE_INDEX) null else position,
                    nextKey = if (listNotes.isEmpty()) null else position + 3
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }
}
