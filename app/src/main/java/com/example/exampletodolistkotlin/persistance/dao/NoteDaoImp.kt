package com.example.exampletodolistkotlin.persistance.dao

import android.provider.ContactsContract.CommonDataKinds.Note
import androidx.lifecycle.LiveData
import com.example.exampletodolistkotlin.baseActivity.ObjectBox
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity_
import io.objectbox.Box
import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.Query


/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */
class NoteDaoImp: NoteDao {
    var noteEntityBox: Box<NoteEntity>

    init {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
    }

    override fun save(noteEntity: NoteEntity) {
        noteEntityBox.put(noteEntity)
    }

    override fun delete(noteEntity: NoteEntity) {
        noteEntityBox.remove(noteEntity)
    }

    override fun getAllNotes(): List<NoteEntity> {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        return noteEntityBox.query().notNull(NoteEntity_.name).build().find()
    }

    override fun getNoteByName(query: String): List<NoteEntity> {
        return noteEntityBox.query().contains(NoteEntity_.name, query).build().find()
    }

    override fun getNoteById(id: Long): NoteEntity? {
        return noteEntityBox.get(id)
    }

    override fun getListNote(): Query<NoteEntity>? {
        return noteEntityBox.query().build()
    }

    override fun pagingNotes(position: Long ,limit: Long): List<NoteEntity>? {
        noteEntityBox = ObjectBox.boxStore.boxFor(NoteEntity::class.java)
        val notesEntities: List<NoteEntity> =
                noteEntityBox.query().notNull(NoteEntity_.id).build().find(position, limit)
        if (notesEntities != null){
            return notesEntities
        }
        return null
    }
}