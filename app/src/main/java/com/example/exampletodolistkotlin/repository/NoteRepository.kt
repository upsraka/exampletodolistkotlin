package com.example.exampletodolistkotlin.repository

import com.example.exampletodolistkotlin.baseActivity.BaseRepo
import com.example.exampletodolistkotlin.persistance.dao.NoteDao
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.Query

/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */
class NoteRepository constructor(
        private val noteDao: NoteDao): BaseRepo{

    fun createNote(noteEntity: NoteEntity) {
        return noteDao.save(noteEntity)
    }

    fun deleteNote(noteEntity: NoteEntity) {
        return noteDao.delete(noteEntity)
    }

    fun getAllNotes(): List<NoteEntity> {
        return noteDao.getAllNotes()
    }

    fun getNoteById(id: Long): NoteEntity? {
        return noteDao.getNoteById(id)
    }

    fun pagingNote(position: Long ,limit: Long): List<NoteEntity>? {
        return noteDao.pagingNotes(position, limit)
    }
}