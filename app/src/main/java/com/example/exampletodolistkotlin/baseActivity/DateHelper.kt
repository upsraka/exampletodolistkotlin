package com.example.exampletodolistkotlin.baseActivity

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Raka Putra on 1/4/21
 * Jakarta, Indonesia.
 */
object DateHelper {
    fun formatDate(calendar: Calendar): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(calendar.time)
    }

}