package com.example.exampletodolistkotlin.page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.example.exampletodolistkotlin.adapter.NotePagingSource
import com.example.exampletodolistkotlin.persistance.dao.NoteDao
import com.example.exampletodolistkotlin.persistance.dao.NoteDaoImp
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import com.example.exampletodolistkotlin.repository.NoteRepository
import io.objectbox.query.Query

/**
 * Created by Raka Putra on 1/3/21
 * Jakarta, Indonesia.
 */
class MainActivityViewModel constructor() : ViewModel() {
    val TAG = MainActivityViewModel::class.java.name
    private val noteDao: NoteDao
    private val repository: NoteRepository
    val getAllNotes: List<NoteEntity>

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }

    init {
        noteDao = NoteDaoImp()
        repository = NoteRepository(noteDao)
        getAllNotes = repository.getAllNotes()
    }

    val noteList = Pager(
            config = PagingConfig(
                    pageSize = NETWORK_PAGE_SIZE,
                    enablePlaceholders = true
            ),
            pagingSourceFactory = { NotePagingSource(noteDao, repository) }
    ).flow.cachedIn(viewModelScope)
}