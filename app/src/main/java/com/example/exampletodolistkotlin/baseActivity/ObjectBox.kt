package com.example.exampletodolistkotlin.baseActivity

import android.content.Context
import android.util.Log
import com.example.exampletodolistkotlin.persistance.entity.MyObjectBox
import io.objectbox.BoxStore
import io.objectbox.android.AndroidObjectBrowser
import io.objectbox.android.BuildConfig

/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */
object ObjectBox {
    lateinit var boxStore: BoxStore

    fun init(context: Context) {
        boxStore = MyObjectBox.builder()
                .androidContext(context.applicationContext)
                .build()

        if (BuildConfig.DEBUG) {
            // Enable Data Browser on debug builds.
            // https://docs.objectbox.io/data-browser
            val started = AndroidObjectBrowser(boxStore).start(context.applicationContext)
            Log.d("Objectbox here", started.toString())
        }


    }
}