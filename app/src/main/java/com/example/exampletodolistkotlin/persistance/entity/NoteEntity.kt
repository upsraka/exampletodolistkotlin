package com.example.exampletodolistkotlin.persistance.entity

import android.os.Parcel
import android.os.Parcelable
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.util.*

/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */

@Entity
class NoteEntity {
    @Id
    var id: Long = 0
    var name: String? = null
    var note: String? = null
    var date: String? = null
    constructor()
    constructor(name: String?, note: String?, date: String) : super() {
        this.name = name
        this.note = note
        this.date = date
    }
}