package com.example.exampletodolistkotlin.persistance.dao

import androidx.lifecycle.LiveData
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import io.objectbox.android.ObjectBoxLiveData
import io.objectbox.query.Query

/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */
interface NoteDao {
    fun save(noteEntity: NoteEntity)

    fun delete(noteEntity: NoteEntity)

    fun getAllNotes(): List<NoteEntity>

    fun getNoteByName(query: String): List<NoteEntity>

    fun getNoteById(id: Long): NoteEntity?

    fun getListNote(): Query<NoteEntity>?

    fun pagingNotes(position: Long ,limit: Long): List<NoteEntity>?
}