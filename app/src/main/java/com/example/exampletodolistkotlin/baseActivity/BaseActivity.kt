package com.example.exampletodolistkotlin.baseActivity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * Created by Raka Putra on 12/29/20
 * Jakarta, Indonesia.
 */
abstract class BaseActivity: AppCompatActivity() {
    protected inline fun <reified T : ViewDataBinding> binding (
            @LayoutRes resId: Int
    ): Lazy<T> = lazy { DataBindingUtil.setContentView<T>(this, resId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
        setupListener()
        setupObserver()
    }

    open fun setupView() {
    }

    open fun setupListener() {
    }

    open fun setupObserver() {
    }

    open fun onSuccess() {
    }

    open fun onError() {
    }
}