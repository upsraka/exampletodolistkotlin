package com.example.exampletodolistkotlin.page

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.viewModels
import com.example.exampletodolistkotlin.R
import com.example.exampletodolistkotlin.baseActivity.BaseActivity
import com.example.exampletodolistkotlin.baseActivity.DateHelper
import com.example.exampletodolistkotlin.baseActivity.ObjectBox
import com.example.exampletodolistkotlin.databinding.ActivityAddEditBinding
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import java.util.*

/**
 * Created by Raka Putra on 1/3/21
 * Jakarta, Indonesia.
 */
class NoteAddActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {
    private val binding: ActivityAddEditBinding by binding(R.layout.activity_add_edit)
    val viewModel: NoteAddViewModel by viewModels()

    private lateinit var datePickerDialog: DatePickerDialog
    private val calendar = Calendar.getInstance()
    private var noteEntity: NoteEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retrieveFromIntent()
    }

    override fun setupView() {
        super.setupView()
        binding.apply {
            lifecycleOwner = this@NoteAddActivity
            noteAddActivity = viewModel
        }
    }

    override fun setupListener() {
        super.setupListener()
        datePickerDialog = DatePickerDialog(
                this, this, calendar.get(Calendar.YEAR), calendar.get(
                Calendar.MONTH
        ), calendar.get(Calendar.DAY_OF_MONTH)
        )

        binding.dateButton.setOnClickListener {
            datePickerDialog.show()
        }

        binding.fabSave.setOnClickListener {
            if (binding.etAddName.text.isNullOrBlank() ||
                    binding.etAddNote.text.isNullOrBlank() ||
                    binding.tvAddDate.text.isNullOrBlank())
                Toast.makeText(this, "Please enter all the required fields", Toast.LENGTH_SHORT).show()
            else {
                val noteItemEntity = NoteEntity(
                        name = binding.etAddName.text.toString(),
                        note = binding.etAddNote.text.toString(),
                        date = binding.tvAddDate.text.toString()
                )
                if (noteEntity != null) {
                    noteItemEntity.id = noteEntity!!.id
                }
                viewModel.save(noteItemEntity)
                finish()
            }
        }
    }

    override fun setupObserver() {
        super.setupObserver()

    }

    override fun onDateSet(datePicker: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        binding.tvAddDate.text = DateHelper.formatDate(calendar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return if (noteEntity != null) {
            val inflater = menuInflater
            inflater.inflate(R.menu.add_edit_menu, menu)
            true
        } else {
            false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.delete -> {
                viewModel.delete(noteEntity!!)
                Toast.makeText(this, "Removed note with title " +
                        noteEntity?.name, Toast.LENGTH_LONG).show()
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun retrieveFromIntent(){
        val noteId = intent.extras?.getLong(MainActivity.INTENT_EXTRA_KEY)
        if (noteId != null) {
            noteEntity = viewModel.getNoteById(noteId)
            binding.etAddName.setText(noteEntity?.name)
            binding.etAddNote.setText(noteEntity?.note)
            binding.tvAddDate.text = noteEntity?.date
        }
    }
}