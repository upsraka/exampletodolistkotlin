package com.example.exampletodolistkotlin.page

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.exampletodolistkotlin.baseActivity.ObjectBox
import com.example.exampletodolistkotlin.persistance.dao.NoteDao
import com.example.exampletodolistkotlin.persistance.dao.NoteDaoImp
import com.example.exampletodolistkotlin.persistance.entity.NoteEntity
import com.example.exampletodolistkotlin.repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Raka Putra on 1/3/21
 * Jakarta, Indonesia.
 */
class NoteAddViewModel constructor() : ViewModel() {
    val TAG = NoteAddViewModel::class.java.name

    private val noteDao: NoteDao
    private val repository: NoteRepository

    val note: LiveData<String> get() = _note
    val _note: MutableLiveData<String> = MutableLiveData()

    init {
        noteDao = NoteDaoImp()
        repository = NoteRepository(noteDao)
    }

    fun delete(noteEntity: NoteEntity) {
        viewModelScope.launch {
            Dispatchers.IO
            repository.deleteNote(noteEntity!!)
        }
    }

    fun save(noteEntity: NoteEntity) {
        viewModelScope.launch {
            repository.createNote(noteEntity!!)
        }
    }

    fun getNoteById(id: Long): NoteEntity? {
        return repository.getNoteById(id)
    }
}